# miniproject-scientific-computing2022
I am Pooria Danaeifar. This is my mini project for Scientific Computing course.
This program converts text to voice using gTTS package.
The pygame library is used for playing mp3 file.
You must select the language you want before converting to voice.
You can open a text file by pressing "Browse" button and convert it to audio(mp3 file).

## Installing Requirements
There are some requierments you should install before use this code. If you don't have each library, use the intallation comand below.
gTTS package:
To install this package run one of the following:
```
conda install -c tdido gtts-token
pip install gTTS
```

ttk package:
To install this package run one of the following:
```
conda install -c anaconda tk
pip install tk
```


ttkbootstrap package:
```
pip install ttkbootstrap
```

pygame package:
To install this package run one of the following:
```
conda install -c cogsci pygame
pip install pygame

```



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gricad-gitlab.univ-grenoble-alpes.fr/danaeifp/miniproject-scientific-computing2022.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gricad-gitlab.univ-grenoble-alpes.fr/danaeifp/miniproject-scientific-computing2022/-/settings/integrations)



## Name
Text to Audio convertor.

## Description
This is a Python code which converts text to audio using gTTS package. You can open a file or write inside the program. This program uses tk as standard Python user interface (UI).

## Visuals
![Alt text](screenshot.png?raw=true "Screenshot")



## License
This code is open source.