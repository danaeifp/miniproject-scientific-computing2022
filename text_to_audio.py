import ttkbootstrap as ttk
import tkinter.font as TkFont

from ttkbootstrap.constants import *
from tkinter.filedialog import askopenfilename
from tkinter.scrolledtext import ScrolledText

from gtts import gTTS
import os
import pygame

from tkinter import messagebox
from pandas import read_excel, concat, DataFrame, ExcelWriter
from os import getcwd

pygame.mixer.init()



class TextReader(ttk.Frame):

    def __init__(self, master):
        super().__init__(master, padding=15)
        self.filename = ttk.StringVar()
        self.pack(fill=BOTH, expand=YES)
        self.create_widget_elements()

    def create_widget_elements(self):
        """Create and add the widget elements"""
        style = ttk.Style()

        # Dark mode:
        def checker():
            if var1.get() == 0:
                style.theme_use('cosmo')
            else:
                style.theme_use('darkly')
        
        var1 = ttk.IntVar()
        mycheck = ttk.Checkbutton(bootstyle="primary, round-toggle",
            text="Light|Dark",
            variable=var1,
            onvalue=1,
            offvalue=0,
            command=checker)
        mycheck.pack(side=RIGHT, fill=X, expand=NO, padx=(0, 15), pady=12)

        # Textbox:
        self.textbox = ScrolledText(
            master=self,
            highlightcolor=style.colors.primary,
            highlightbackground=style.colors.border,
            highlightthickness=1
        )

        helv11 = TkFont.Font(family="Helvetica",size=11,weight="bold")
        self.textbox.configure(font = helv11)
        self.textbox.pack(fill=BOTH)

        # Language selection:
        self.gtlang = {
            "English": "en",
            "French": "fr",
            "German": "de"
        } 
        # Combobox
        self.selected_lang = ttk.StringVar()
        langcb = ttk.Combobox(textvariable = self.selected_lang)
        langcb['values'] = list(self.gtlang.keys())
        langcb['state'] = 'readonly'
        langcb.current(0)
        langcb.pack(side=LEFT, padx=(15, 10), pady=10)
        # langcb.pack(side=LEFT, fill=X, expand=NO, padx=(0, 5), pady=12)

        default_txt0 = {
            "English": "Click the Browse button to open a new text file.",
            "French": "Cliquez sur le bouton Browse pour ouvrir un nouveau fichier texte.",
            "German": 'Klicken Sie auf die Schaltfläche Browse, um eine neue Textdatei zu öffnen.'
        } 

        default_txt = default_txt0[self.selected_lang.get()]
        self.textbox.insert(END, default_txt)

        file_entry = ttk.Entry(self, textvariable=self.filename)
        file_entry.pack(side=LEFT, fill=X, expand=YES, padx=(0, 5), pady=10)


        browse_btn = ttk.Button(self, text="Browse", command=self.open_file)
        browse_btn.pack(side=RIGHT, fill=X, padx=(5, 0), pady=10)

        play_btn = ttk.Button(self, text="Play", command=self.createbtn)
        play_btn.pack(side=RIGHT, fill=X, padx=(5, 0), pady=10)




    

    def sectinglang(self):
        s_lng = self.selected_lang.get()
        return self.gtlang[s_lng]

    def open_file(self):
        path = askopenfilename()
        if not path:
            return

        with open(path, encoding='utf-8') as f:
            self.textbox.delete('1.0', END)
            self.textbox.insert(END, f.read())
            self.filename.set(path)



    pygame.mixer.init()
    def createbtn(self):
        mytext = self.textbox.get("1.0", END)
        lng = self.sectinglang()
        audio = gTTS(text=mytext, lang=lng, slow=False)
        audio.save("example.mp3")
        music_path = os.getcwd()
        print(music_path)
        pygame.mixer.music.load(f"{music_path}/example.mp3")
        pygame.mixer.music.play(loops = 0)




if __name__ == '__main__':

    app = ttk.Window("Text Reader", "sandstone")
    TextReader(app)
    app.mainloop()